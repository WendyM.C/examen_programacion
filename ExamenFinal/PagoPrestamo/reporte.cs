﻿using PagoPrestamo.entitie;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace PagoPrestamo
{
    public partial class reporte : Form
    {
        public List<DatoReporte> datos = new List<DatoReporte>();
        public reporte()
        {
            InitializeComponent();
        }

        private void reporte_Load(object sender, EventArgs e)
        {

            this.reportViewer1.LocalReport.ReportEmbeddedResource = "PagoPrestamo.Report1.rdlc";
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", datos));
            this.reportViewer1.RefreshReport();


        }
    }
}
