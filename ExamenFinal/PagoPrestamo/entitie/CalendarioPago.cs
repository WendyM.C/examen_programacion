﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagoPrestamo.entitie
{
    class CalendarioPago
    {
        public double MontoPrestamo { get; set; }
        public int Plazo { get; set; }
        public double Interes { get; set; }
        public DateTime Fecha { get; set; }

        public CalendarioPago() { }

        public CalendarioPago(double montoPrestamo, int plazo, double interes, DateTime fecha)
        {
            MontoPrestamo = montoPrestamo;
            Plazo = plazo;
            Interes = interes;
            Fecha = fecha;
        }

    }
}
