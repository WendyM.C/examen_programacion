﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagoPrestamo.entitie
{
    public class DatoReporte
    {

        public int NoCuota { get; set; }
       
        public double Interes { get; set; }
        public double Principal { get; set; }
        public double Cuota { get; set; }
        public double Pago { get; set; }
        public DateTime Fecha { get; set; }

        public DatoReporte()
        {
        }

        public DatoReporte(int noCuota, double interes, double principal, double cuota, double pago, DateTime fecha)
        {
            NoCuota = noCuota;
            Interes = interes;
            Principal = principal;
            Cuota = cuota;
            Pago = pago;
            Fecha = fecha;
        }
    }
}
