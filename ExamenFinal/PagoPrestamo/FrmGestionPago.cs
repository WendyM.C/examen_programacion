﻿using PagoPrestamo.entitie;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PagoPrestamo
{
    public partial class FrmGestionPago : Form
    {
        private int contador;
        private DataSet dsCuotapago;
        private BindingSource bsCuotapago;

        public FrmGestionPago()
        {
            InitializeComponent();
        }

        public DataSet DsPagoCalendario
        {
            set
            {
                dsCuotapago = value;
            }
        }
        
        public BindingSource BsPagoCalendario

        {
            set
            {
                bsCuotapago = value;
            }
        }

        private void TxtMonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            numerosDecimales(e, txtMontoprestamo);
        }

        public void numerosDecimales(KeyPressEventArgs v, TextBox t)
        {
            if (char.IsDigit(v.KeyChar)) v.Handled = false;
            else if (char.IsSeparator(v.KeyChar)) v.Handled = false;
            else if (char.IsControl(v.KeyChar)) v.Handled = false;
            else if (v.KeyChar.ToString().Equals("."))
            {

                for (int i = 0; i < t.Text.Length; i++)
                {
                    if (t.Text.Substring(i, 1).Equals(".")) ++contador;
                }

                contador++;

                if (contador > 1)
                {
                    v.Handled = true;
                    contador = 0;
                }
                else v.Handled = false;
            }
            else v.Handled = true;
        }

        public void numeros(KeyPressEventArgs v)
        {
            if (char.IsDigit(v.KeyChar)) v.Handled = false;
            else if (char.IsSeparator(v.KeyChar)) v.Handled = false;
            else if (char.IsControl(v.KeyChar)) v.Handled = false;
            else v.Handled = true;
        }

        private void TxtInteres_KeyPress(object sender, KeyPressEventArgs e)
        {
            numerosDecimales(e, txtTasaInteres);

        }

        private void TxtPlazo_KeyPress(object sender, KeyPressEventArgs e)
        {
            numeros(e);
        }

        private void BtnCalcular_Click(object sender, EventArgs e)
        {
            setTabla();
        }

        public void setTabla()
        {
            if (isNull())
            {
                MessageBox.Show("Campos inválidos", "Mensaje de error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            int plazosTotal = Int32.Parse(txtPlazopago.Text) * 12;
            DateTime dt = dtpFechaInicio.Value;
            double tazaPeriodoMensual = (Double.Parse(txtTasaInteres.Text) / 100) / 12;

            double cuotaNumerador = Double.Parse(txtMontoprestamo.Text) * (tazaPeriodoMensual *
                Math.Pow((1 + tazaPeriodoMensual), plazosTotal));

            double cuotaMensual = cuotaNumerador /
                (Math.Pow((1 + tazaPeriodoMensual), plazosTotal) - 1);

            double saldo = Double.Parse(txtMontoprestamo.Text);

            int contador = 0;
            for (int i = 1; i <= plazosTotal; i++)
            {
                contador = contador + 30;
                double interes = saldo * tazaPeriodoMensual;
                double principal = cuotaMensual - interes;
                saldo -= principal;

                DataRow drPagoprestamo = dsCuotapago.Tables["TablaPrestamo"].NewRow();
                drPagoprestamo["NoCuota"] = i;
                drPagoprestamo["Cuota"] = cuotaMensual;
                drPagoprestamo["Principal"] = principal;
                drPagoprestamo["Pago"] = saldo;
                drPagoprestamo["Interes"] = interes;
                drPagoprestamo["Fecha"] = dt.AddDays(contador);

                dsCuotapago.Tables["TablaPrestamo"].Rows.Add(drPagoprestamo);
                bsCuotapago.DataSource = dsCuotapago;
                bsCuotapago.DataMember = dsCuotapago.Tables["TablaPrestamo"].TableName;

                dgvPagoPrestamo.DataSource = bsCuotapago;
            }
        }

        private bool isNull()
        {
            if (txtTasaInteres.Text == "" || txtMontoprestamo.Text == "" || txtPlazopago.Text == "")
            {
                return true;
            }

            return false;
        }

        private void txtMonto_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmGestionPago_Load(object sender, EventArgs e)
        {

        }

        private void butreport_Click(object sender, EventArgs e)
        {
            int rowCount = dgvPagoPrestamo.Rows.Count;
            //validando si se agregaron productos al datagridview
            if (rowCount <= 0)
            {
                MessageBox.Show(this, "Primero debe de calcular", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            reporte rep = new reporte();

            for (int i = 0; i < dgvPagoPrestamo.Rows.Count; i++)
            {
                DatoReporte pago = new DatoReporte();

                pago.NoCuota = (int)this.dgvPagoPrestamo.Rows[i].Cells[0].Value;
                pago.Fecha = (DateTime)this.dgvPagoPrestamo.Rows[i].Cells[1].Value;
                pago.Interes = (double)this.dgvPagoPrestamo.Rows[i].Cells[2].Value;
                pago.Principal = (double)this.dgvPagoPrestamo.Rows[i].Cells[3].Value;
                pago.Cuota = (double)this.dgvPagoPrestamo.Rows[i].Cells[4].Value;
                pago.Pago = (double)this.dgvPagoPrestamo.Rows[i].Cells[5].Value;
                rep.datos.Add(pago);
            }
            rep.ShowDialog();

        }
    }
}
    
